# Advent of Code 2019 in Lua

Each folder contains a solution as a single `main.lua` source file which computes both parts of the
day’s challenge for the `input` file.

Simply [install Lua](https://www.lua.org/start.html), then run
`lua main.lua < input` (you can replace `input` with the file containing your input) in the
folder of the day you’re interested in. The executable then prints the two solutions in the
terminal.
