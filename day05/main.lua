function get_value(program, inst, idx, param_num)
  local param = program[idx + param_num]
  local mode = (inst // (100 * 10 ^ (param_num - 1))) % 10
  if mode == 1 then
    return param
  else
    return program[param]
  end
end

function execute(original_program, input)
  local dbg_count = 0
  local idx = 0
  local program = {}
  for orig_key, orig_value in pairs(original_program) do
    program[orig_key] = orig_value
  end

  local output = nil
  while true do
    local inst = program[idx]
    local opcode = (inst % 100)

    if opcode == 1 then
      program[program[idx + 3]] = get_value(program, inst, idx, 1) + get_value(program, inst, idx, 2)
      idx = idx + 4
    elseif opcode == 2 then
      program[program[idx + 3]] = get_value(program, inst, idx, 1) * get_value(program, inst, idx, 2)
      idx = idx + 4
    elseif opcode == 3 then
      program[program[idx + 1]] = input
      idx = idx + 2
    elseif opcode == 4 then
      output = get_value(program, inst, idx, 1)
      idx = idx + 2
    elseif opcode == 5 then
      if get_value(program, inst, idx, 1) ~= 0 then
        idx = get_value(program, inst, idx, 2)
      else
        idx = idx + 3
      end
    elseif opcode == 6 then
      if get_value(program, inst, idx, 1) == 0 then
        idx = get_value(program, inst, idx, 2)
      else
        idx = idx + 3
      end
    elseif opcode == 7 then
      local less_than
      if get_value(program, inst, idx, 1) < get_value(program, inst, idx, 2) then
         less_than = 1
      else
         less_than = 0
      end
      program[program[idx + 3]] = less_than
      idx = idx + 4
    elseif opcode == 8 then
      local equal
      if get_value(program, inst, idx, 1) == get_value(program, inst, idx, 2) then
         equal = 1
      else
         equal = 0
      end
      program[program[idx + 3]] = equal
      idx = idx + 4
    else
      return output
    end
  end
end

local program = {}
local idx = 0
for num in string.gmatch(io.read(), "%-?%d+") do
  program[idx] = tonumber(num)
  idx = idx + 1
end

print("Part One: " .. execute(program, 1))
print("Part Two: " .. execute(program, 5))
