function orbit_count(orbit_map, orbit, count)
  result = count
  if orbit_map[orbit] ~= nil then
    for _, orbitting in pairs(orbit_map[orbit]) do
      result = result + orbit_count(orbit_map, orbitting, count + 1)
    end
  end
  return result
end

function get_path(orbit_map, orbit, goal)
  if orbit == goal then
    return {}
  elseif orbit_map[orbit] == nil then
    return nil
  end

  for _, orbitting in pairs(orbit_map[orbit]) do
    local path = get_path(orbit_map, orbitting, goal)
    if path ~= nil then
      table.insert(path, 1, orbit)
      return path
    end
  end
end

function orbital_transfer_count(orbit_map, goal1, goal2)
  local path1 = get_path(orbit_map, "COM", goal1)
  local path2 = get_path(orbit_map, "COM", goal2)

  local idx = 2
  while path1[idx] == path2[idx] do
    idx = idx + 1
  end

  return (#path1 - idx + 1) + (#path2 - idx + 1)
end

local orbit_map = {}
for line in io.lines() do
  local semicolon_position = line:find(")")
  local orbit = line:sub(1, semicolon_position - 1)
  local orbitting = line:sub(semicolon_position + 1)

  if orbit_map[orbit] == nil then
    orbit_map[orbit] = {}
  end
  table.insert(orbit_map[orbit], orbitting)
end

print("Part One: " .. orbit_count(orbit_map, "COM", 0))
print("Part Two: " .. orbital_transfer_count(orbit_map, "YOU", "SAN"))
