function recursive_fuel(fuel)
  local fuel_for_fuel = (fuel // 3) - 2
  if fuel_for_fuel <= 0 then
    return fuel
  else
    return fuel + recursive_fuel(fuel_for_fuel)
  end
end

local fuel_part_one = 0
local fuel_part_two = 0

while true do
  local line = io.read("*number")
  if line == nil then break end
  local fuel = (line // 3) - 2
  fuel_part_one = fuel_part_one + fuel
  fuel_part_two = fuel_part_two + recursive_fuel(fuel)
end

print("Part One: " .. fuel_part_one)
print("Part Two: " .. fuel_part_two)
