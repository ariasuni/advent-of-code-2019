function execute(original_program, noun, verb)
  local idx = 0
  local program = {}
  for orig_key, orig_value in pairs(original_program) do
    program[orig_key] = orig_value
  end
  program[1] = noun
  program[2] = verb

  while true do
    if program[idx] == 1 then
      program[program[idx + 3]] = program[program[idx + 1]] + program[program[idx + 2]]
    elseif program[idx] == 2 then
      program[program[idx + 3]] = program[program[idx + 1]] * program[program[idx + 2]]
    else
      return program[0]
    end
    idx = idx + 4
  end
end

function execute_until_result(program)
  for noun = 0, 99 do
    for verb = 0, 99 do
      local result = execute(program, noun, verb)
      if result == 19690720 then
        return 100 * noun + verb
      end
    end
  end
end

local program = {}
local idx = 0
for num in string.gmatch(io.read(), "%d+") do
  -- we don’t use table.insert because we want to begin at zero
  program[idx] = tonumber(num)
  idx = idx + 1
end

local part_one = execute(program, 12, 2)
local part_two = execute_until_result(program)

print("Part One: " .. part_one)
print("Part Two: " .. part_two)
