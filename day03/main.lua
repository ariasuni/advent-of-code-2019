local positions = {}
for wire = 1, 2 do
  local position = {x=0, y=0}
  local steps = 0
  positions[wire] = {}
  for inst in string.gmatch(io.read(), "[A-Z]%d+") do
    local direction = inst:sub(1, 1)
    local distance = tonumber(inst:sub(2))

    local direction_axis
    local direction_mult
    if direction == "L" then
      direction_axis = "x"
      direction_mult = -1
    elseif direction == "R" then
      direction_axis = "x"
      direction_mult = 1
    elseif direction == "U" then
      direction_axis = "y"
      direction_mult = -1
    else -- direction == "D"
      direction_axis = "y"
      direction_mult = 1
    end

    for i = 1, distance do
      position[direction_axis] = position[direction_axis] + direction_mult
      steps = steps + 1

      local position_string = position.x .. ";" .. position.y
      positions[wire][position_string] = positions[wire][position_string] or steps
    end
  end
end

local shortest_distance = nil
local minimum_steps = nil
for k in pairs(positions[1]) do
  if positions[2][k] ~= nil then
    local semicolon_position = k:find(";")
    local x = k:sub(1, semicolon_position - 1)
    local y = k:sub(semicolon_position + 1)

    local distance = math.abs(tonumber(x)) + math.abs(tonumber(y))
    if shortest_distance == nil or distance < shortest_distance then
      shortest_distance = distance
    end

    local steps = positions[1][k] + positions[2][k]
    if minimum_steps == nil or steps < minimum_steps then
      minimum_steps = steps
    end
  end
end

print("Part One: " .. shortest_distance)
print("Part Two: " .. minimum_steps)
