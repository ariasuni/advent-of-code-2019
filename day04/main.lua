local input = io.read()

local dash_position = input:find("-")
local start = tonumber(input:sub(1, dash_position - 1))
local ending = tonumber(input:sub(dash_position + 1))

local valid_password_count_one = 0
local valid_password_count_two = 0

for password = start, ending do
  password_s = tostring(password)
  local prev_c = "!" -- ! is before 0 in the ascii table
  local has_double = false
  local duplicate_letter = nil
  local duplicate_count = 0
  local is_increasing = true

  for c in password_s:gmatch"." do
    if c == prev_c then
      if duplicate_letter == nil then
        duplicate_letter = c
        duplicate_count = 2
      else
        duplicate_count = duplicate_count + 1
      end
    else
      if c < prev_c then
        is_increasing = false
        break
      end

      duplicate_letter = nil
      if duplicate_count == 2 then
        has_double = true
      end
    end

    prev_c = c
  end

  if duplicate_count == 2 then
    has_double = true
  end

  if is_increasing and duplicate_count >= 2 then
    valid_password_count_one = valid_password_count_one + 1
    if has_double then
      valid_password_count_two = valid_password_count_two + 1
    end
  end
end

print("Part One: " .. valid_password_count_one)
print("Part Two: " .. valid_password_count_two)
